package com.grpc.gprc_client_server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GprcClientServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(GprcClientServerApplication.class, args);
	}

}
